package activity;

public class Contact {
    private String name;
    private String contactNumber;
    private String address;

    public Contact() {

    }

    public Contact(String name, String contactNumber, String address){
        this.address = address;
        this.name = name;
        this.contactNumber = contactNumber;
    }

     public String getName(){
        return name;
     }

     public String getContactNumber(){
        return contactNumber;
     }

     public String getAddress(){
        return address;
     }
}
