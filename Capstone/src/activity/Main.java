package activity;

public class Main {
    public static void main(String[]args){
        Contact contact = new Contact("John Doe", "+639152468596", "Quezon City");
        Contact contact2 = new Contact("jane Doe", "+639162148573", "Caloocan City");
//        Method 1
        Phonebook phonebook1 = new Phonebook();
        phonebook1.addContact(contact);
        phonebook1.addContact(contact2);

//        Method 2
        Phonebook phonebook2 = new Phonebook(contact2);
        phonebook2.addContact(contact);

//        Method 1
        for (Contact x : phonebook1.getContacts()) {
            System.out.println("   ");
            System.out.println(x.getName());
            System.out.println("=============================");
            System.out.println(x.getName() + " has the following registered number:");
            System.out.println(x.getContactNumber());
            System.out.println(x.getName() + " has the following registered address:");
            System.out.println(x.getAddress());
        }

        System.out.println("\n -------- Method 2 ----------");

//        Method 2
        for (Contact x : phonebook2.getContacts()) {
            System.out.println("   ");
            System.out.println(x.getName());
            System.out.println("=============================");
            System.out.println(x.getName() + " has the following registered number:");
            System.out.println(x.getContactNumber());
            System.out.println(x.getName() + " has the following registered address:");
            System.out.println(x.getAddress());
        }
    }
}
