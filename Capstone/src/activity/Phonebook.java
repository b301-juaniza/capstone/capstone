package activity;

import java.util.ArrayList;

public class Phonebook extends Contact{
    private ArrayList<Contact> contacts = new ArrayList<>();

    public Phonebook(){

    }

    public Phonebook(Contact contact){
        super(contact.getName(), contact.getContactNumber(), contact.getAddress());
        this.contacts.add(contact);
    }

//    "John Doe", "+639152468596", "Quezon City"
    public void addContact(Contact contact){
        contacts.add(contact);
    }

    public ArrayList<Contact> getContacts(){
        return contacts;
    }
}
